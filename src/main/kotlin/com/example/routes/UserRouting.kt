package com.example.routes

import com.example.models.User
import com.example.models.userList
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File

fun Route.userRouting() {
    route("/users") {
        get {
            if (userList.isNotEmpty()) {
                call.respond(userList)
            } else {
                call.respondText("No users found", status = HttpStatusCode.OK)
            }
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (user in userList) {
                if (user.id == id) return@get call.respond(user)
            }
            call.respondText( "No user with id $id", status = HttpStatusCode.NotFound)
        }
        post {
//            val user = call.receive<User>()
//            userList.add(user)
//            call.respondText("User stored correctly", status = HttpStatusCode.Created)
            var userId = ""
            var userName = ""
            var userProfilePic = ""

            // Aquí recollim les dades que ens envia el client
            val data = call.receiveMultipart()
            // Separem el tractament de les dades entre: dades primitives i fitxes
            data.forEachPart { part ->
                when(part) {
                    // Aquí recollim les dades primitives
                    is PartData.FormItem -> {
                        when(part.name) {
                            "id" -> userId = part.value
                            "name" -> userName = part.value
                        }
                    }
                    // Aquí recollim els fitxes
                    is PartData.FileItem -> {
                        userProfilePic = "uploads/" + part.originalFileName as String
                        val fileBytes = part.streamProvider().readBytes()
                        File(userProfilePic).writeBytes(fileBytes)
                    }
                    else -> {}
                }

            }

            userList.add(User(userId, userName, userProfilePic))
            call.respondText("User stored correctly", status = HttpStatusCode.Created)

        }
        get("/profilePic/{id?}") {
            var file: File = File("")
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (user in userList) {
                if (user.id == id) file = File(user.profilePic)
            }
            if (file.exists()) {
                call.respondFile(file)
            } else {
                call.respondText("No image found", status = HttpStatusCode.NotFound)
            }
         }
    }
}
